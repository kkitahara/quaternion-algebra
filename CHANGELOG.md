v1.0.4:
- Minor update: README.md.

v1.0.3:
- Minor update: .npmignore, README.md.

v1.0.2:
- Added .npmignore.
- Added CHNAGELOG.md.
