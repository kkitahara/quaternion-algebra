/**
 * @source: https://www.npmjs.com/package/@kkitahara/quaternion-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @typedef {number|Polynomial} RealAlgebraicElement
 *
 * @desc
 * A RealAlgebraicElement denotes a {@link number}
 * or a {@link Polynomial} as follows.
 * * A {@link number} for the numerical algebra.
 * * A {@link Polynomial} for the exact algebra.
 *
 * See the documents of @kkitahara/real-algebra for details.
 */

/**
 * @desc
 * The Quaternion class is a class for quaternions.
 *
 * @version 1.0.0
 * @since 1.0.0
 */
export class Quaternion {
  /**
   * @desc
   * The constructor function of the {@link Quaternion} class.
   *
   * CAUTION: this constructor function does not check if `a`, `b`, `c` and `d`
   * are valid {@link RealAlgebraicElement}s.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} b
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} c
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} d
   * a {@link RealAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Quaternion as H } from '@kkitahara/quaternion-algebra'
   *
   * let a = new H(1, 2, 3, 4)
   * a.re === 1 // true
   * a.im[0] === 2 // true
   * a.im[1] === 3 // true
   * a.im[2] === 4 // true
   */
  constructor (a, b, c, d) {
    /**
     * @desc
     * The Quaternion#re is the real part of `this` {@link Quaternion}.
     *
     * @type {RealAlgebraicElement}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.re = a
    /**
     * @desc
     * The Quaternion#im is the imaginary part of `this` {@link Quaternion}.
     * The imaginary part is an array of length `3`.
     *
     * @type {Array}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.im = [b, c, d]
  }

  /**
   * @desc
   * The Quaternion#toString method converts
   * `this` to a human-readable {@link string}.
   *
   * @param {number} [radix]
   * the base to use for representing numeric values.
   *
   * @return {string}
   * a human-readable {@link string} representation of `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H } from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   *
   * new H(1, 2, 3, 4).toString() // '1 + i(2) + j(3) + k(4)'
   * new H(0, 0, 0, 0).toString() // '0'
   * new H(0, 0, 0, 1).toString() // 'k(1)'
   * new H(1, 0, 0, 1).toString() // '1 + k(1)'
   * new H(0, 0, 0, r.$(1, 2, 5)).toString() // 'k((1 / 2)sqrt(5))'
   * new H(1, 0, 0, r.$(1, 2)).toString() // '1 + k(1 / 2)'
   * new H(1, 0, 0, r.$(0)).toString() // '1'
   * new H(r.$(1, 3), 0, 0, 1).toString() // '1 / 3 + k(1)'
   * new H(r.$(0), 0, 1, 0).toString() // 'j(1)'
   */
  toString (radix) {
    let s = ''
    if (typeof this.re === 'number') {
      if (this.re !== 0) {
        s += this.re.toString(radix)
      }
    } else if (!this.re.isZero()) {
      s += this.re.toString(radix)
    }
    const units = ['i', 'j', 'k']
    for (let i = 0; i < 3; i += 1) {
      if (typeof this.im[i] === 'number') {
        if (this.im[i] !== 0) {
          if (s.length === 0) {
            s += units[i] + '(' + this.im[i].toString(radix) + ')'
          } else {
            s += ' + ' + units[i] + '(' + this.im[i].toString(radix) + ')'
          }
        }
      } else if (!this.im[i].isZero()) {
        if (s.length === 0) {
          s += units[i] + '(' + this.im[i].toString(radix) + ')'
        } else {
          s += ' + ' + units[i] + '(' + this.im[i].toString(radix) + ')'
        }
      }
    }
    if (s.length === 0) {
      s = '0'
    }
    return s
  }

  /**
   * @desc
   * The Quaternion#toFixed method returns
   * the fixed-point representation of `this`.
   *
   * @param {number} [digits]
   * the number of digits appear after the decimal point.
   *
   * @return {string}
   * the fixed-point representation of `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H } from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   *
   * let a = r.iadd(r.$(-1, 2), r.$(-1, 2, 5))
   * let b = r.iadd(r.$(-1), a)
   * let z = new H(a, b, 3, 4)
   * z.toFixed(3) // '-1.618 + i(-2.618) + j(3.000) + k(4.000)'
   * new H(0, 0, 0, 0).toFixed(3) // '0.000'
   * new H(0, 0, 0, 1).toFixed(3) // 'k(1.000)'
   * new H(0, 0, 0, r.$(1)).toFixed(3) // 'k(1.000)'
   * new H(1, 0, 0, r.$(1)).toFixed(3) // '1.000 + k(1.000)'
   * new H(r.$(0), 0, 0, r.$(0)).toFixed(3) // '0.000'
   */
  toFixed (digits) {
    let s = ''
    if (typeof this.re === 'number') {
      if (this.re !== 0) {
        s = s + this.re.toFixed(digits)
      }
    } else if (!this.re.isZero()) {
      s = s + this.re.toFixed(digits)
    }
    const units = ['i', 'j', 'k']
    for (let i = 0; i < 3; i += 1) {
      if (typeof this.im[i] === 'number') {
        if (this.im[i] !== 0) {
          if (s.length === 0) {
            s = s + units[i] + '(' + this.im[i].toFixed(digits) + ')'
          } else {
            s = s + ' + ' + units[i] + '(' + this.im[i].toFixed(digits) + ')'
          }
        }
      } else if (!this.im[i].isZero()) {
        if (s.length === 0) {
          s = s + units[i] + '(' + this.im[i].toFixed(digits) + ')'
        } else {
          s = s + ' + ' + units[i] + '(' + this.im[i].toFixed(digits) + ')'
        }
      }
    }
    if (s.length === 0) {
      s = this.re.toFixed(digits)
    }
    return s
  }

  /**
   * @desc
   * The Quaternion#toJSON method converts
   * `this` to an object serialisable by `JSON.stringify`.
   *
   * @return {object}
   * a serialisable object for `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H } from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   *
   * let a = r.iadd(r.$(-1, 2), r.$(-1, 2, 5))
   * let b = r.iadd(r.$(-1), a)
   * let z = new H(a, b, 3, 4)
   *
   * // toJSON method is called by JSON.stringify
   * let s = JSON.stringify(z)
   *
   * typeof s // 'string'
   */
  toJSON () {
    const obj = {}
    obj.reviver = 'Quaternion'
    obj.version = '1.0.0'
    obj.re = this.re
    obj.im = this.im
    return obj
  }

  /**
   * @desc
   * The Quaternion.reviver function converts
   * the data produced by {@link Quaternion#toJSON} to a {@link Quaternion}.
   *
   * @param {object} key
   *
   * @param {object} value
   *
   * @return {Quaternion|Object}
   * a {@link Quaternion} or `value`.
   *
   * @throws {Error}
   * if the given object is invalid.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Quaternion as H } from '@kkitahara/quaternion-algebra'
   *
   * let a = 1 / 2 * Math.sqrt(5)
   * let b = a - 1
   * let z = new H(a, b, 3, 4)
   * let s = JSON.stringify(z)
   *
   * let w = JSON.parse(s, H.reviver)
   *
   * typeof s // 'string'
   * w instanceof H // true
   * w !== z // true
   * z.re === w.re // true
   * z.im[0] === w.im[0] // true
   * z.im[1] === w.im[1] // true
   * z.im[2] === w.im[2] // true
   *
   * let s2 = s.replace('1.0.0', '0.0.0')
   * JSON.parse(s2, H.reviver) // Error
   */
  static reviver (key, value) {
    if (value !== null && typeof value === 'object' &&
        value.reviver === 'Quaternion') {
      if (value.version === '1.0.0') {
        return new Quaternion(value.re, ...value.im)
      } else {
        throw Error('invalid version.')
      }
    } else {
      return value
    }
  }
}

/* @license-end */
