/**
 * @source: https://www.npmjs.com/package/@kkitahara/quaternion-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Quaternion } from './quaternion.mjs'

/**
 * @desc
 * The QuaternionAlgebra class is a class for quaternion algebra.
 *
 * @version 1.0.0
 * @since 1.0.0
 *
 * @example
 * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
 * import { QuaternionAlgebra } from '@kkitahara/quaternion-algebra'
 * let r = new RealAlgebra()
 * let h = new QuaternionAlgebra(r)
 * let q1, q2, q3
 *
 * // Generate a new quaternion
 * q1 = h.$(1, 2, 3, 4)
 * q1.toString() // '1 + i(2) + j(3) + k(4)'
 *
 * q1 = h.$(0, 0, 0, r.$(1, 2, 5))
 * q1.toString() // 'k((1 / 2)sqrt(5))'
 *
 * // Real and imaginary parts
 * q1 = h.$(1, 2, 3, 4)
 * q1.re.toString() // '1'
 * q1.im instanceof Array // true
 * q1.im[0].toString() // '2'
 * q1.im[1].toString() // '3'
 * q1.im[2].toString() // '4'
 *
 * // Copy (create a new object)
 * q1 = h.$(1, 2, 3, 4)
 * q2 = h.copy(q1)
 * q2.toString() // '1 + i(2) + j(3) + k(4)'
 *
 * // Equality
 * q1 = h.$(1, 2, 3, 4)
 * q2 = h.$(1, -2, 3, 4)
 * h.eq(q1, q2) // false
 *
 * q2 = h.$(1, 2, 3, 4)
 * h.eq(q1, q2) // true
 *
 * // Inequality
 * q1 = h.$(1, 2, 3, 4)
 * q2 = h.$(1, -2, 3, 4)
 * h.ne(q1, q2) // true
 *
 * q2 = h.$(1, 2, 3, 4)
 * h.ne(q1, q2) // false
 *
 * // isZero
 * h.isZero(h.$(0, 0, 0, 0)) // true
 * h.isZero(h.$(1, 0, 0, 0)) // false
 * h.isZero(h.$(0, 1, 0, 0)) // false
 * h.isZero(h.$(0, 0, 1, 0)) // false
 * h.isZero(h.$(0, 0, 0, 1)) // false
 *
 * // isInteger
 * h.isInteger(h.$(1, 2, 3, 4)) // true
 * h.isInteger(h.$(1, r.$(2, 3), 3, 4)) // false
 * h.isInteger(h.$(1, r.$(4, 2), 3, 4)) // true
 *
 * // Addition
 * q1 = h.$(1, 2, 3, 4)
 * q2 = h.$(1, 1, 2, 1)
 * // new object is generated
 * q3 = h.add(q1, q2)
 * q3.toString() // '2 + i(3) + j(5) + k(5)'
 *
 * // In-place addition
 * q1 = h.$(1, 2, 3, 4)
 * q2 = h.$(1, 1, 2, 1)
 * // new object is not generated
 * q1 = h.iadd(q1, q2)
 * q1.toString() // '2 + i(3) + j(5) + k(5)'
 *
 * // Subtraction
 * q1 = h.$(1, 2, 3, 4)
 * q2 = h.$(1, 1, 2, 1)
 * // new object is generated
 * q3 = h.sub(q1, q2)
 * q3.toString() // 'i(1) + j(1) + k(3)'
 *
 * // In-place subtraction
 * q1 = h.$(1, 2, 3, 4)
 * q2 = h.$(1, 1, 2, 1)
 * // new object is not generated
 * q1 = h.isub(q1, q2)
 * q1.toString() // 'i(1) + j(1) + k(3)'
 *
 * // Maltiplication
 * q1 = h.$(1, 2, 3, 4)
 * q2 = h.$(1, -2, -3, -4)
 * // new object is generated
 * q3 = h.mul(q1, q2)
 * q3.toString() // '30'
 *
 * // some fundametal products
 * h.mul(h.$(1, 0, 0, 0), h.$(0, 1, 0, 0)).toString() // 'i(1)'
 * h.mul(h.$(0, 1, 0, 0), h.$(1, 0, 0, 0)).toString() // 'i(1)'
 * h.mul(h.$(1, 0, 0, 0), h.$(0, 0, 1, 0)).toString() // 'j(1)'
 * h.mul(h.$(0, 0, 1, 0), h.$(1, 0, 0, 0)).toString() // 'j(1)'
 * h.mul(h.$(1, 0, 0, 0), h.$(0, 0, 0, 1)).toString() // 'k(1)'
 * h.mul(h.$(0, 0, 0, 1), h.$(1, 0, 0, 0)).toString() // 'k(1)'
 * h.mul(h.$(1, 0, 0, 0), h.$(1, 0, 0, 0)).toString() // '1'
 * h.mul(h.$(0, 1, 0, 0), h.$(0, 1, 0, 0)).toString() // '-1'
 * h.mul(h.$(0, 0, 1, 0), h.$(0, 0, 1, 0)).toString() // '-1'
 * h.mul(h.$(0, 0, 0, 1), h.$(0, 0, 0, 1)).toString() // '-1'
 * h.mul(h.$(0, 1, 0, 0), h.$(0, 0, 1, 0)).toString() // 'k(1)'
 * h.mul(h.$(0, 0, 1, 0), h.$(0, 1, 0, 0)).toString() // 'k(-1)'
 * h.mul(h.$(0, 0, 1, 0), h.$(0, 0, 0, 1)).toString() // 'i(1)'
 * h.mul(h.$(0, 0, 0, 1), h.$(0, 0, 1, 0)).toString() // 'i(-1)'
 * h.mul(h.$(0, 0, 0, 1), h.$(0, 1, 0, 0)).toString() // 'j(1)'
 * h.mul(h.$(0, 1, 0, 0), h.$(0, 0, 0, 1)).toString() // 'j(-1)'
 *
 * // In-place multiplication
 * q1 = h.$(1, 2, 3, 4)
 * q2 = h.$(1, -2, -3, -4)
 * // new object is not generated
 * q1 = h.imul(q1, q2)
 * q1.toString() // '30'
 *
 * // Division
 * q1 = h.$(1, 2, 3, 4)
 * q2 = h.$(1, 2, 3, 4)
 * // new object is generated
 * q3 = h.div(q1, q2)
 * q3.toString() // '1'
 *
 * // In-place division
 * q1 = h.$(1, 2, 3, 4)
 * q2 = h.$(1, 2, 3, 4)
 * // new object is not generated
 * q1 = h.idiv(q1, q2)
 * q1.toString() // '1'
 *
 * // Multiplication by -1
 * q1 = h.$(1, 2, 3, 4)
 * // new object is generated
 * q2 = h.neg(q1)
 * q2.toString() // '-1 + i(-2) + j(-3) + k(-4)'
 *
 * // In-place multiplication by -1
 * q1 = h.$(1, 2, 3, 4)
 * // new object is not generated
 * q1 = h.ineg(q1)
 * q1.toString() // '-1 + i(-2) + j(-3) + k(-4)'
 *
 * // Conjugate
 * q1 = h.$(1, 2, 3, 4)
 * // new object is generated
 * q2 = h.cjg(q1)
 * q2.toString() // '1 + i(-2) + j(-3) + k(-4)'
 *
 * // In-place evaluation of the conjugate
 * q1 = h.$(1, 2, 3, 4)
 * // new object is not generated
 * q1 = h.icjg(q1)
 * q1.toString() // '1 + i(-2) + j(-3) + k(-4)'
 *
 * // Square of the absolute value
 * q1 = h.$(1, 2, 3, 4)
 * let a = h.abs2(q1)
 * a.toString() // '30'
 * // return value is not a quaternion (but a real number)
 * a.re // undefined
 * a.im // undefined
 *
 * // JSON (stringify and parse)
 * q1 = h.$(1, 2, 3, 4)
 * let str = JSON.stringify(q1)
 * q2 = JSON.parse(str, h.reviver)
 * h.eq(q1, q2) // true
 */
export class QuaternionAlgebra {
  /**
   * @desc
   * The constructor of the {@link QuaternionAlgebra} class.
   *
   * CAUTION: this function does not check if `ralg` is a valid implementation
   * of {@link RealAlgebra}.
   *
   * @param {RealAlgebra} ralg
   * an instance of {@link RealAlgebra}.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  constructor (ralg) {
    /**
     * @desc
     * The QuaternionAlgebra#ralg is used
     * to manipurate each coefficient of {@link Quaternion}s.
     *
     * @type {RealAlgebra}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.ralg = ralg
  }

  /**
   * @desc
   * The QuaternionAlgebra#$ method returns a new instance of
   * {@link Quaternion} representing `a + bi + cj + dk`.
   * It uses copies of `a`, `b`, `c` and `d`.
   *
   * @param {RealAlgebraicElement} [a = 0]
   * real part.
   *
   * @param {RealAlgebraicElement} [b = 0]
   * the first imaginary part.
   *
   * @param {RealAlgebraicElement} [c = 0]
   * the second imaginary part.
   *
   * @param {RealAlgebraicElement} [d = 0]
   * the third imaginary part.
   *
   * @return {Quaternion}
   * a {@link Quaternion} representing
   * `a + bi + cj + dk`
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q = h.$(1, 2, 3, 4)
   * q instanceof H // true
   * r.eq(q.re, 1) // true
   * r.eq(q.im[0], 2) // true
   * r.eq(q.im[1], 3) // true
   * r.eq(q.im[2], 4) // true
   *
   * q = h.$()
   * q instanceof H // true
   * r.eq(q.re, 0) // true
   * r.eq(q.im[0], 0) // true
   * r.eq(q.im[1], 0) // true
   * r.eq(q.im[2], 0) // true
   */
  $ (a = 0, b = 0, c = 0, d = 0) {
    const ralg = this.ralg
    a = ralg.copy(a)
    b = ralg.copy(b)
    c = ralg.copy(c)
    d = ralg.copy(d)
    return new Quaternion(a, b, c, d)
  }

  /**
   * @desc
   * The QuaternionAlgebra#cast method just returns
   * `q` if `q` is a {@link Quaternion}
   * and all the coefficients of `q` are valid instances of
   * {@link RealAlgebraicElement},
   * and otherwise casts `q` to a {@link Quaternion}.
   *
   * @param {object} q
   * an object.
   *
   * @return {Quaternion}
   * `q` or a new {@link Quaternion}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 0, 0, 0)
   * let q2 = q1
   *
   * q2 === h.cast(q1) // true
   * h.cast(1) instanceof H // true
   * q2 === h.cast(1) // false
   * h.eq(q1, h.cast(1)) // true
   *
   * h.eq(h.cast({ re: 2, im: [1, 2, 3] }), new H(2, 1, 2, 3)) // true
   */
  cast (q) {
    if (!(q instanceof Quaternion)) {
      const ralg = this.ralg
      const re = q.re
      const im = q.im
      if (re !== undefined || im !== undefined) {
        q = new Quaternion(ralg.cast(re), ralg.cast(im[0]), ralg.cast(im[1]),
          ralg.cast(im[2]))
      } else {
        q = new Quaternion(ralg.cast(q), ralg.$(0), ralg.$(0), ralg.$(0))
      }
    } else {
      const ralg = this.ralg
      const re = ralg.cast(q.re)
      const imi = ralg.cast(q.im[0])
      const imj = ralg.cast(q.im[1])
      const imk = ralg.cast(q.im[2])
      if (re !== q.re || imi !== q.im[0] || imj !== q.im[1] ||
        imk !== q.im[2]) {
        q = new q.constructor(re, imi, imj, imk)
      }
    }
    return q
  }

  /**
   * @desc
   * The QuaternionAlgebra#copy method returns a copy of `q`.
   *
   * @param {Quaternion} q
   * a {@link Quaternion}.
   *
   * @return {Quaternion}
   * a copy of `q`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 0, 0, 0)
   * let q2 = h.copy(q1)
   *
   * q1 instanceof H // true
   * q2 instanceof H // true
   * r.eq(q1.re, q2.re) // true
   * r.eq(q1.im[0], q2.im[0]) // true
   * r.eq(q1.im[1], q2.im[1]) // true
   * r.eq(q1.im[2], q2.im[2]) // true
   * q1 !== q2 // true
   * q1.re !== q2.re // true
   * q1.im !== q2.im // true
   * q1.im[0] !== q2.im[0] // true
   * q1.im[1] !== q2.im[1] // true
   * q1.im[2] !== q2.im[2] // true
   */
  copy (q) {
    q = this.cast(q)
    const ralg = this.ralg
    return new q.constructor(ralg.copy(q.re), ralg.copy(q.im[0]),
      ralg.copy(q.im[1]), ralg.copy(q.im[2]))
  }

  /**
   * @desc
   * The QuaternionAlgebra#eq method checks if `q1` is equal to `q2`.
   *
   * @param {Quaternion} q1
   * a {@link Quaternion}.
   *
   * @param {Quaternion} q2
   * a {@link Quaternion}.
   *
   * @return {boolean}
   * `true` if `q1` is equal to `q2` and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { QuaternionAlgebra } from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q = h.$(r.$(1, 2, 5), 2, 3, 4)
   *
   * h.eq(q, h.$(r.$(1, 2, 5), 2, 3, 4)) // true
   * h.eq(q, h.$(1, 2, 3, 4)) // false
   * h.eq(q, h.$(r.$(-1, 2, 5), 2, 3, 4)) // false
   */
  eq (q1, q2) {
    q1 = this.cast(q1)
    q2 = this.cast(q2)
    const ralg = this.ralg
    return ralg.eq(q1.re, q2.re) && ralg.eq(q1.im[0], q2.im[0]) &&
      ralg.eq(q1.im[1], q2.im[1]) && ralg.eq(q1.im[2], q2.im[2])
  }

  /**
   * @desc
   * The QuaternionAlgebra#ne method checks if `q1` is not equal to `q2`.
   *
   * `halg.ne(q1, q2)` is an alias for `!halg.eq(q1, q2)`,
   * where `halg` is an instance of {@link QuaternionAlgebra}.
   *
   * @param {Quaternion} q1
   * a {@link Quaternion}.
   *
   * @param {Quaternion} q2
   * a {@link Quaternion}.
   *
   * @return {boolean}
   * `true` if `q1` is not equal to `q2` and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { QuaternionAlgebra } from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q = h.$(r.$(1, 2, 5), 2, 3, 4)
   *
   * h.ne(q, h.$(r.$(1, 2, 5), 2, 3, 4)) // false
   * h.ne(q, h.$(1, 2, 3, 4)) // true
   * h.ne(q, h.$(r.$(-1, 2, 5), 2, 3, 4)) // true
   */
  ne (q1, q2) {
    return !this.eq(q1, q2)
  }

  /**
   * @desc
   * The QuaternionAlgebra#isZero method checks if `q` is zero.
   *
   * @param {Quaternion} q
   * a {@link Quaternion}.
   *
   * @return {boolean}
   * `true` if `q` is zero and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { QuaternionAlgebra } from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * h.isZero(h.$(r.$(0, 2, 5), 0, 0, 0)) // true
   * h.isZero(h.$(r.$(1, 2, 5), 0, 0, 0)) // false
   * h.isZero(h.$(0, 0, r.$(-1, 2, 5), 0)) // false
   */
  isZero (q) {
    q = this.cast(q)
    const ralg = this.ralg
    return ralg.isZero(q.re) && ralg.isZero(q.im[0]) && ralg.isZero(q.im[1]) &&
      ralg.isZero(q.im[2])
  }

  /**
   * @desc
   * The QuaternionAlgebra#isInteger method checks if all the coefficients
   * of `q` are integers.
   *
   * @param {Quaternion} q
   * a {@link Quaternion}.
   *
   * @return {boolean}
   * `true` if all the coefficients of `q` are integers
   * and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { QuaternionAlgebra } from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * h.isInteger(h.$(r.$(0, 2, 5), 2, 3, 4)) // true
   * h.isInteger(h.$(1, r.$(1, 2, 5), 3, 4)) // false
   * h.isInteger(h.$(1, 2, 3, r.$(1, 2, 5))) // false
   * h.isInteger(h.$(1, 2, r.$(4, 2, 1), 4)) // true
   */
  isInteger (q) {
    q = this.cast(q)
    const ralg = this.ralg
    return ralg.isInteger(q.re) && ralg.isInteger(q.im[0]) &&
      ralg.isInteger(q.im[1]) && ralg.isInteger(q.im[2])
  }

  /**
   * @desc
   * The QuaternionAlgebra#isFinite method checks if `q` is finite.
   *
   * @param {Quaternion} q
   * a {@link Quaternion}.
   *
   * @return {boolean}
   * `true` if `q` is finite and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { QuaternionAlgebra } from '@kkitahara/quaternion-algebra'
   * import bigInt from 'big-integer'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let a = bigInt('1e999')
   * h.isFinite(h.$(r.$(1, 2, 5), 2, 3, 4)) // true
   * h.isFinite(h.$(r.$(a, 2, 5), 2, 3, 4)) // true
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { QuaternionAlgebra } from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let a = 1e999
   * h.isFinite(h.$(r.$(1, 2, 5), 2, 3, 4)) // true
   * h.isFinite(h.$(r.$(a, 2, 5), 2, 3, 4)) // false
   */
  isFinite (q) {
    q = this.cast(q)
    const ralg = this.ralg
    return ralg.isFinite(q.re) && ralg.isFinite(q.im[0]) &&
      ralg.isFinite(q.im[1]) && ralg.isFinite(q.im[2])
  }

  /**
   * @desc
   * The QuaternionAlgebra#isExact method checks
   * if `this` is an implementation of exact algebra.
   *
   * @return {boolean}
   * `true` if `this` is an exact algebra and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { QuaternionAlgebra } from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * h.isExact() // true
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { QuaternionAlgebra } from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * h.isExact() // false
   */
  isExact () {
    return this.ralg.isExact()
  }

  /**
   * @desc
   * The QuaternionAlgebra#isReal method checks
   * if `this` is an implementation of real algebra.
   *
   * @return {boolean}
   * `false`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { QuaternionAlgebra } from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * r.isReal() // true
   * h.isReal() // false
   */
  isReal () {
    return false
  }

  /**
   * @desc
   * The QuaternionAlgebra#add method returns the result of
   * the addition `q1` plus `q2`.
   *
   * `halg.add(q1, q2)` is an alias for `halg.iadd(halg.copy(q1), q2)`,
   * where `halg` is an instance of {@link QuaternionAlgebra}.
   *
   * @param {Quaternion} q1
   * a {@link Quaternion}.
   *
   * @param {Quaternion} q2
   * a {@link Quaternion}.
   *
   * @return {Quaternion}
   * a new {@link Quaternion}
   * representing the result of the addition `q1` plus `q2`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, 3, 4)
   * let q2 = h.$(2, 3, -4, 5)
   *
   * let q3 = h.add(q1, q2)
   * q3 instanceof H // true
   * q3 !== q1 // true
   * h.eq(q1, new H(1, 2, 3, 4)) // true
   * h.eq(q3, new H(3, 5, -1, 9)) // true
   */
  add (q1, q2) {
    return this.iadd(this.copy(q1), q2)
  }

  /**
   * @desc
   * The QuaternionAlgebra#iadd method adds `q2` to `q1` *in place*.
   *
   * @param {Quaternion} q1
   * a {@link Quaternion}.
   *
   * @param {Quaternion} q2
   * a {@link Quaternion}.
   *
   * @return {Quaternion}
   * `q1`, or a new {@link Quaternion} if `q1` is not a {@link Quaternion}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, 3, 4)
   * let q2 = h.$(2, 3, -4, 5)
   * let q3 = q1
   *
   * // GOOD-PRACTICE!
   * q1 = h.iadd(q1, q2)
   * q1 === q3 // true
   * q1 instanceof H // true
   * h.eq(q1, new H(3, 5, -1, 9)) // true
   */
  iadd (q1, q2) {
    q1 = this.cast(q1)
    q2 = this.cast(q2)
    const ralg = this.ralg
    q1.re = ralg.iadd(q1.re, q2.re)
    q1.im[0] = ralg.iadd(q1.im[0], q2.im[0])
    q1.im[1] = ralg.iadd(q1.im[1], q2.im[1])
    q1.im[2] = ralg.iadd(q1.im[2], q2.im[2])
    return q1
  }

  /**
   * @desc
   * The QuaternionAlgebra#sub method returns the result of
   * the subtraction `q1` minus `q2`.
   *
   * `halg.sub(q1, q2)` is an alias for `halg.isub(halg.copy(q1), q2)`,
   * where `halg` is an instance of {@link QuaternionAlgebra}.
   *
   * @param {Quaternion} q1
   * a {@link Quaternion}.
   *
   * @param {Quaternion} q2
   * a {@link Quaternion}.
   *
   * @return {Quaternion}
   * a new {@link Quaternion}
   * representing the result of the subtraction `q1` minus `q2`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, 3, 4)
   * let q2 = h.$(2, 3, -4, 5)
   *
   * let q3 = h.sub(q1, q2)
   * q3 instanceof H // true
   * q3 !== q1 // true
   * h.eq(q1, new H(1, 2, 3, 4)) // true
   * h.eq(q3, new H(-1, -1, 7, -1)) // true
   */
  sub (q1, q2) {
    return this.isub(this.copy(q1), q2)
  }

  /**
   * @desc
   * The QuaternionAlgebra#isub method subtracts `q2` from `q1` *in place*.
   *
   * @param {Quaternion} q1
   * a {@link Quaternion}.
   *
   * @param {Quaternion} q2
   * a {@link q2}.
   *
   * @return {Quaternion}
   * `q1`, or a new {@link Quaternion} if `q1` is not a
   * {@link Quaternion}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, 3, 4)
   * let q2 = h.$(2, 3, -4, 5)
   * let q3 = q1
   *
   * // GOOD-PRACTICE!
   * q1 = h.isub(q1, q2)
   * q1 === q3 // true
   * q1 instanceof H // true
   * h.eq(q1, new H(-1, -1, 7, -1)) // true
   */
  isub (q1, q2) {
    q1 = this.cast(q1)
    q2 = this.cast(q2)
    const ralg = this.ralg
    q1.re = ralg.isub(q1.re, q2.re)
    q2.im[0] = ralg.isub(q1.im[0], q2.im[0])
    q2.im[1] = ralg.isub(q1.im[1], q2.im[1])
    q2.im[2] = ralg.isub(q1.im[2], q2.im[2])
    return q1
  }

  /**
   * @desc
   * The QuaterionAlgebra#mul method returns the result of
   * the multiplication `q1` times `q2`.
   *
   * `halg.mul(q1, q2)` is an alias for `halg.imul(halg.copy(q1), q2)`,
   * where `halg` is an instance of {@link QuaternionAlgebra}.
   *
   * @param {Quaternion} q1
   * a {@link Quaternion}.
   *
   * @param {Quaternion} q2
   * a {@link Quaternion}.
   *
   * @return {Quaternion}
   * a new {@link Quaternion}
   * representing the result of the multiplication `q1` times `q2`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, 3, 4)
   * let q2 = h.$(1, -2, -3, -4)
   *
   * let q3 = h.mul(q1, q2)
   * q3 instanceof H // true
   * q3 !== q1 // true
   * h.eq(q1, new H(1, 2, 3, 4)) // true
   * h.eq(q3, new H(30, 0, 0, 0)) // true
   */
  mul (q1, q2) {
    return this.imul(this.copy(q1), q2)
  }

  /**
   * @desc
   * The QuaternionAlgebra#imul method multiplies `q1` by `q2` *in place*.
   *
   * Convension of the multiplication is as follows:
   * * i * i = -1
   * * j * j = -1
   * * k * k = -1
   * * i * j = -j * i = k
   * * j * k = -k * j = i
   * * k * i = -i * k = j
   *
   * @param {Quaternion} q1
   * a {@link Quaternion}.
   *
   * @param {Quaternion} q2
   * a {@link Quaternion}.
   *
   * @return {Quaternion}
   * `q1`, or a new {@link Quaternion} if `q1` is not a
   * {@link Quaternion}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, 3, 4)
   * let q2 = h.$(1, -2, -3, -4)
   * let q3 = q1
   *
   * // GOOD-PRACTICE!
   * q1 = h.imul(q1, q2)
   * q1 === q3 // true
   * q1 instanceof H // true
   * h.eq(q1, new H(30, 0, 0, 0)) // true
   */
  imul (q1, q2) {
    q1 = this.cast(q1)
    q2 = this.cast(q2)
    const ralg = this.ralg
    const a2 = q2.re
    const b2 = q2.im[0]
    const c2 = q2.im[1]
    const d2 = q2.im[2]
    const a1b2 = ralg.mul(q1.re, b2)
    const a1c2 = ralg.mul(q1.re, c2)
    const a1d2 = ralg.mul(q1.re, d2)
    const b1b2 = ralg.mul(q1.im[0], b2)
    const b1c2 = ralg.mul(q1.im[0], c2)
    const b1d2 = ralg.mul(q1.im[0], d2)
    const c1b2 = ralg.mul(q1.im[1], b2)
    const c1c2 = ralg.mul(q1.im[1], c2)
    const c1d2 = ralg.mul(q1.im[1], d2)
    const d1b2 = ralg.mul(q1.im[2], b2)
    const d1c2 = ralg.mul(q1.im[2], c2)
    const d1d2 = ralg.mul(q1.im[2], d2)
    q1.re = ralg.imul(q1.re, a2)
    q1.re = ralg.isub(q1.re, b1b2)
    q1.re = ralg.isub(q1.re, c1c2)
    q1.re = ralg.isub(q1.re, d1d2)
    q1.im[0] = ralg.imul(q1.im[0], a2)
    q1.im[0] = ralg.iadd(q1.im[0], a1b2)
    q1.im[0] = ralg.iadd(q1.im[0], c1d2)
    q1.im[0] = ralg.isub(q1.im[0], d1c2)
    q1.im[1] = ralg.imul(q1.im[1], a2)
    q1.im[1] = ralg.iadd(q1.im[1], a1c2)
    q1.im[1] = ralg.iadd(q1.im[1], d1b2)
    q1.im[1] = ralg.isub(q1.im[1], b1d2)
    q1.im[2] = ralg.imul(q1.im[2], a2)
    q1.im[2] = ralg.iadd(q1.im[2], a1d2)
    q1.im[2] = ralg.iadd(q1.im[2], b1c2)
    q1.im[2] = ralg.isub(q1.im[2], c1b2)
    return q1
  }

  /**
   * @desc
   * The QuaternionAlgebra#div method returns the result of
   * the division `q1` over `q2`.
   *
   * `halg.div(q1, q2)` is an alias for `halg.idiv(halg.copy(q1), q2)`,
   * where `halg` is an instance of {@link QuaternionAlgebra}.
   *
   * @param {Quaternion} q1
   * a {@link Quaternion}.
   *
   * @param {Quaternion} q2
   * a {@link Quaternion}.
   *
   * @return {Quaternion}
   * a new {@link Quaternion}
   * representing the result of the division `q1` over `q2`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, 3, 4)
   * let q2 = h.$(1, 2, 3, 4)
   *
   * let q3 = h.div(q1, q2)
   * q3 instanceof H // true
   * q3 !== q1 // true
   * h.eq(q1, new H(1, 2, 3, 4)) // true
   * h.eq(q3, new H(1, 0, 0, 0)) // true
   */
  div (q1, q2) {
    return this.idiv(this.copy(q1), q2)
  }

  /**
   * @desc
   * The QuaternionAlgebra#idiv method divides `q1` by `q2` *in place*.
   *
   * @param {Quaternion} q1
   * a {@link Quaternon}.
   *
   * @param {Quaternion} q2
   * a {@link Quaternion}.
   *
   * @return {Quaternion}
   * `q1`, or a new {@link Quaternion} if `q1` is not a
   * {@link Quaternion}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, 3, 4)
   * let q2 = h.$(1, 2, 3, 4)
   * let q3 = q1
   *
   * // GOOD-PRACTICE!
   * q1 = h.idiv(q1, q2)
   * q1 === q3 // true
   * q1 instanceof H // true
   * h.eq(q1, new H(1, 0, 0, 0)) // true
   */
  idiv (q1, q2) {
    q1 = this.cast(q1)
    q2 = this.cast(q2)
    const abs2 = this.abs2(q2)
    const ralg = this.ralg
    const a2 = q2.re
    const b2 = q2.im[0]
    const c2 = q2.im[1]
    const d2 = q2.im[2]
    const a1b2 = ralg.mul(q1.re, b2)
    const a1c2 = ralg.mul(q1.re, c2)
    const a1d2 = ralg.mul(q1.re, d2)
    const b1b2 = ralg.mul(q1.im[0], b2)
    const b1c2 = ralg.mul(q1.im[0], c2)
    const b1d2 = ralg.mul(q1.im[0], d2)
    const c1b2 = ralg.mul(q1.im[1], b2)
    const c1c2 = ralg.mul(q1.im[1], c2)
    const c1d2 = ralg.mul(q1.im[1], d2)
    const d1b2 = ralg.mul(q1.im[2], b2)
    const d1c2 = ralg.mul(q1.im[2], c2)
    const d1d2 = ralg.mul(q1.im[2], d2)
    q1.re = ralg.imul(q1.re, a2)
    q1.re = ralg.iadd(q1.re, b1b2)
    q1.re = ralg.iadd(q1.re, c1c2)
    q1.re = ralg.iadd(q1.re, d1d2)
    q1.re = ralg.idiv(q1.re, abs2)
    q1.im[0] = ralg.imul(q1.im[0], a2)
    q1.im[0] = ralg.isub(q1.im[0], a1b2)
    q1.im[0] = ralg.isub(q1.im[0], c1d2)
    q1.im[0] = ralg.iadd(q1.im[0], d1c2)
    q1.im[0] = ralg.idiv(q1.im[0], abs2)
    q1.im[1] = ralg.imul(q1.im[1], a2)
    q1.im[1] = ralg.isub(q1.im[1], a1c2)
    q1.im[1] = ralg.isub(q1.im[1], d1b2)
    q1.im[1] = ralg.iadd(q1.im[1], b1d2)
    q1.im[1] = ralg.idiv(q1.im[1], abs2)
    q1.im[2] = ralg.imul(q1.im[2], a2)
    q1.im[2] = ralg.isub(q1.im[2], a1d2)
    q1.im[2] = ralg.isub(q1.im[2], b1c2)
    q1.im[2] = ralg.iadd(q1.im[2], c1b2)
    q1.im[2] = ralg.idiv(q1.im[2], abs2)
    return q1
  }

  /**
   * @desc
   * The QuaternonAlgebra#neg method returns the result of
   * the multiplication `q1` times `-1`.
   *
   * `halg.neg(q)` is an alias for `halg.ineg(halg.copy(q))`,
   * where `halg` is an instance of {@link QuaternionAlgebra}.
   *
   * @param {Quaternion} q
   * a {@link Quaternion}.
   *
   * @return {Quaternion}
   * a new {@link Quaternion}
   * representing the result of the multiplication `q` times `-1`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, 3, 4)
   *
   * let q2 = h.neg(q1)
   * q2 instanceof H // true
   * q2 !== q1 // true
   * h.eq(q1, new H(1, 2, 3, 4)) // true
   * h.eq(q2, new H(-1, -2, -3, -4)) // true
   */
  neg (q) {
    return this.ineg(this.copy(q))
  }

  /**
   * @desc
   * The QuaternionAlgebra#ineg method multiplies `q` by `-1` *in place*.
   *
   * @param {Quaternion} q
   * a {@link Quaternion}.
   *
   * @return {Quaternion}
   * `q`, or a new {@link Quaternion} if `q` is not a
   * {@link Quaternion}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, 3, 4)
   * let q2 = q1
   *
   * // GOOD-PRACTICE!
   * q1 = h.ineg(q1)
   * q1 === q2 // true
   * q1 instanceof H // true
   * h.eq(q1, new H(-1, -2, -3, -4)) // true
   */
  ineg (q) {
    q = this.cast(q)
    const ralg = this.ralg
    q.re = ralg.ineg(q.re)
    q.im[0] = ralg.ineg(q.im[0])
    q.im[1] = ralg.ineg(q.im[1])
    q.im[2] = ralg.ineg(q.im[2])
    return q
  }

  /**
   * @desc
   * The QuaternionAlgebra#cjg method returns the conjugate of `q`.
   *
   * `halg.cjg(q)` is an alias for `halg.icjg(halg.copy(q))`,
   * where `halg` is an instance of {@link QuaternionAlgebra}.
   *
   * @param {Quaternion} q
   * a {@link Quaternion}.
   *
   * @return {Quaternion}
   * a new {@link Quaternion}
   * representing the conjugate of `q`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, 3, 4)
   *
   * let q2 = h.cjg(q1)
   * q2 instanceof H // true
   * q2 !== q1 // true
   * h.eq(q1, new H(1, 2, 3, 4)) // true
   * h.eq(q2, new H(1, -2, -3, -4)) // true
   */
  cjg (q) {
    return this.icjg(this.copy(q))
  }

  /**
   * @desc
   * The QuaternionAlgebra#icjg method
   * evaluates the conjugate of `q` and stores the result to `q`.
   * The conjugate of *a* + *b*i + *c*j + *d*k is
   * *a* - *b*i - *c*j - *d*k.
   *
   * @param {Quaternion} q
   * a {@link Quaternion}.
   *
   * @return {Quaternion}
   * `q`, or a new {@link Quaternion} if `q` is not a
   * {@link Quaternion}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, 3, 4)
   * let q2 = q1
   *
   * // GOOD-PRACTICE!
   * q1 = h.icjg(q1)
   * q1 === q2 // true
   * q1 instanceof H // true
   * h.eq(q1, new H(1, -2, -3, -4)) // true
   */
  icjg (q) {
    q = this.cast(q)
    q.im[0] = this.ralg.ineg(q.im[0])
    q.im[1] = this.ralg.ineg(q.im[1])
    q.im[2] = this.ralg.ineg(q.im[2])
    return q
  }

  /**
   * @desc
   * The QuaternionAlgebra#abs method returns the absolute value of `q`.
   *
   * CAUTION: this method is not implemented for exact algebra.
   *
   * @param {Quaternion} q
   * a {@link Quaternion}.
   *
   * @return {RealAlgebraicElement}
   * a {@link RealAlgebraicElement}
   * representing the absolute value of `q`.
   *
   * @throws {Error}
   * if `this` is an implementation of exact algebra.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, 3, 4)
   *
   * h.abs(q1) // Error
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q = h.$(1, 2, 3, 4)
   *
   * let a = h.abs(q)
   * a instanceof H // false
   * typeof a === 'number' // true
   * h.eq(q, new H(1, 2, 3, 4)) // true
   * r.eq(a, Math.sqrt(30)) // true
   */
  abs (q) {
    if (this.isExact()) {
      throw Error('`abs` is not implemented for exact algebra.')
    } else {
      return Math.sqrt(this.abs2(q))
    }
  }

  /**
   * @desc
   * The QuaternionAlgebra#abs2 method returns
   * the square of the absolute value of `q`.
   *
   * @param {Quaternion} q
   * a {@link Quaternion}.
   *
   * @return {RealAlgebraicElement}
   * a {@link RealAlgebraicElement}
   * representing the square of the absolute value of `q`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * import { Quaternion as H, QuaternionAlgebra }
   *   from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q = h.$(1, 2, 3, 4)
   *
   * let a = h.abs2(q)
   * a instanceof H // false
   * a instanceof P // true
   * h.eq(q, new H(1, 2, 3, 4)) // true
   * r.eq(a, 30) // true
   */
  abs2 (q) {
    q = this.cast(q)
    const ralg = this.ralg
    return ralg.iadd(ralg.iadd(ralg.iadd(ralg.abs2(q.re), ralg.abs2(q.im[0])),
      ralg.abs2(q.im[1])), ralg.abs2(q.im[2]))
  }

  /**
   * @desc
   * The reviver function for the {@link Quaternion}s.
   *
   * @type {Function}
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { QuaternionAlgebra } from '@kkitahara/quaternion-algebra'
   * let r = new RealAlgebra()
   * let h = new QuaternionAlgebra(r)
   *
   * let q1 = h.$(1, 2, r.$(1, 2, 5), 4)
   * let str = JSON.stringify(q1)
   * let q2 = JSON.parse(str, h.reviver)
   * h.eq(q1, q2) // true
   */
  get reviver () {
    const ralg = this.ralg
    return function (key, value) {
      value = ralg.reviver(key, value)
      value = Quaternion.reviver(key, value)
      return value
    }
  }
}

/* @license-end */
