import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { QuaternionAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

let q = h.$(r.$(1, 2, 5), 2, 3, 4)

testDriver.test(() => { return h.ne(q, h.$(r.$(1, 2, 5), 2, 3, 4)) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra#ne-example0_0', false)
testDriver.test(() => { return h.ne(q, h.$(1, 2, 3, 4)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#ne-example0_1', false)
testDriver.test(() => { return h.ne(q, h.$(r.$(-1, 2, 5), 2, 3, 4)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#ne-example0_2', false)
