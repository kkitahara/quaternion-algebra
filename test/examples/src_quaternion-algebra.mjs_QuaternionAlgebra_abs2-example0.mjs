import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
  from '@kkitahara/real-algebra'
import { Quaternion as H, QuaternionAlgebra }
  from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

let q = h.$(1, 2, 3, 4)

let a = h.abs2(q)
testDriver.test(() => { return a instanceof H }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra#abs2-example0_0', false)
testDriver.test(() => { return a instanceof P }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#abs2-example0_1', false)
testDriver.test(() => { return h.eq(q, new H(1, 2, 3, 4)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#abs2-example0_2', false)
testDriver.test(() => { return r.eq(a, 30) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#abs2-example0_3', false)
