import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { QuaternionAlgebra }
  from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

let q1 = h.$(1, 2, 3, 4)

testDriver.test(() => { return h.abs(q1) }, Error, 'src/quaternion-algebra.mjs~QuaternionAlgebra#abs-example0_0', false)
