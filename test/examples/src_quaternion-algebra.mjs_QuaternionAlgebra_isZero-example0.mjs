import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { QuaternionAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

testDriver.test(() => { return h.isZero(h.$(r.$(0, 2, 5), 0, 0, 0)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isZero-example0_0', false)
testDriver.test(() => { return h.isZero(h.$(r.$(1, 2, 5), 0, 0, 0)) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isZero-example0_1', false)
testDriver.test(() => { return h.isZero(h.$(0, 0, r.$(-1, 2, 5), 0)) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isZero-example0_2', false)
