import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { QuaternionAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)
let q1, q2, q3

// Generate a new quaternion
q1 = h.$(1, 2, 3, 4)
testDriver.test(() => { return q1.toString() }, '1 + i(2) + j(3) + k(4)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_0', false)

q1 = h.$(0, 0, 0, r.$(1, 2, 5))
testDriver.test(() => { return q1.toString() }, 'k((1 / 2)sqrt(5))', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_1', false)

// Real and imaginary parts
q1 = h.$(1, 2, 3, 4)
testDriver.test(() => { return q1.re.toString() }, '1', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_2', false)
testDriver.test(() => { return q1.im instanceof Array }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_3', false)
testDriver.test(() => { return q1.im[0].toString() }, '2', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_4', false)
testDriver.test(() => { return q1.im[1].toString() }, '3', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_5', false)
testDriver.test(() => { return q1.im[2].toString() }, '4', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_6', false)

// Copy (create a new object)
q1 = h.$(1, 2, 3, 4)
q2 = h.copy(q1)
testDriver.test(() => { return q2.toString() }, '1 + i(2) + j(3) + k(4)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_7', false)

// Equality
q1 = h.$(1, 2, 3, 4)
q2 = h.$(1, -2, 3, 4)
testDriver.test(() => { return h.eq(q1, q2) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_8', false)

q2 = h.$(1, 2, 3, 4)
testDriver.test(() => { return h.eq(q1, q2) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_9', false)

// Inequality
q1 = h.$(1, 2, 3, 4)
q2 = h.$(1, -2, 3, 4)
testDriver.test(() => { return h.ne(q1, q2) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_10', false)

q2 = h.$(1, 2, 3, 4)
testDriver.test(() => { return h.ne(q1, q2) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_11', false)

// isZero
testDriver.test(() => { return h.isZero(h.$(0, 0, 0, 0)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_12', false)
testDriver.test(() => { return h.isZero(h.$(1, 0, 0, 0)) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_13', false)
testDriver.test(() => { return h.isZero(h.$(0, 1, 0, 0)) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_14', false)
testDriver.test(() => { return h.isZero(h.$(0, 0, 1, 0)) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_15', false)
testDriver.test(() => { return h.isZero(h.$(0, 0, 0, 1)) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_16', false)

// isInteger
testDriver.test(() => { return h.isInteger(h.$(1, 2, 3, 4)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_17', false)
testDriver.test(() => { return h.isInteger(h.$(1, r.$(2, 3), 3, 4)) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_18', false)
testDriver.test(() => { return h.isInteger(h.$(1, r.$(4, 2), 3, 4)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_19', false)

// Addition
q1 = h.$(1, 2, 3, 4)
q2 = h.$(1, 1, 2, 1)
// new object is generated
q3 = h.add(q1, q2)
testDriver.test(() => { return q3.toString() }, '2 + i(3) + j(5) + k(5)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_20', false)

// In-place addition
q1 = h.$(1, 2, 3, 4)
q2 = h.$(1, 1, 2, 1)
// new object is not generated
q1 = h.iadd(q1, q2)
testDriver.test(() => { return q1.toString() }, '2 + i(3) + j(5) + k(5)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_21', false)

// Subtraction
q1 = h.$(1, 2, 3, 4)
q2 = h.$(1, 1, 2, 1)
// new object is generated
q3 = h.sub(q1, q2)
testDriver.test(() => { return q3.toString() }, 'i(1) + j(1) + k(3)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_22', false)

// In-place subtraction
q1 = h.$(1, 2, 3, 4)
q2 = h.$(1, 1, 2, 1)
// new object is not generated
q1 = h.isub(q1, q2)
testDriver.test(() => { return q1.toString() }, 'i(1) + j(1) + k(3)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_23', false)

// Maltiplication
q1 = h.$(1, 2, 3, 4)
q2 = h.$(1, -2, -3, -4)
// new object is generated
q3 = h.mul(q1, q2)
testDriver.test(() => { return q3.toString() }, '30', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_24', false)

// some fundametal products
testDriver.test(() => { return h.mul(h.$(1, 0, 0, 0), h.$(0, 1, 0, 0)).toString() }, 'i(1)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_25', false)
testDriver.test(() => { return h.mul(h.$(0, 1, 0, 0), h.$(1, 0, 0, 0)).toString() }, 'i(1)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_26', false)
testDriver.test(() => { return h.mul(h.$(1, 0, 0, 0), h.$(0, 0, 1, 0)).toString() }, 'j(1)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_27', false)
testDriver.test(() => { return h.mul(h.$(0, 0, 1, 0), h.$(1, 0, 0, 0)).toString() }, 'j(1)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_28', false)
testDriver.test(() => { return h.mul(h.$(1, 0, 0, 0), h.$(0, 0, 0, 1)).toString() }, 'k(1)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_29', false)
testDriver.test(() => { return h.mul(h.$(0, 0, 0, 1), h.$(1, 0, 0, 0)).toString() }, 'k(1)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_30', false)
testDriver.test(() => { return h.mul(h.$(1, 0, 0, 0), h.$(1, 0, 0, 0)).toString() }, '1', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_31', false)
testDriver.test(() => { return h.mul(h.$(0, 1, 0, 0), h.$(0, 1, 0, 0)).toString() }, '-1', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_32', false)
testDriver.test(() => { return h.mul(h.$(0, 0, 1, 0), h.$(0, 0, 1, 0)).toString() }, '-1', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_33', false)
testDriver.test(() => { return h.mul(h.$(0, 0, 0, 1), h.$(0, 0, 0, 1)).toString() }, '-1', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_34', false)
testDriver.test(() => { return h.mul(h.$(0, 1, 0, 0), h.$(0, 0, 1, 0)).toString() }, 'k(1)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_35', false)
testDriver.test(() => { return h.mul(h.$(0, 0, 1, 0), h.$(0, 1, 0, 0)).toString() }, 'k(-1)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_36', false)
testDriver.test(() => { return h.mul(h.$(0, 0, 1, 0), h.$(0, 0, 0, 1)).toString() }, 'i(1)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_37', false)
testDriver.test(() => { return h.mul(h.$(0, 0, 0, 1), h.$(0, 0, 1, 0)).toString() }, 'i(-1)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_38', false)
testDriver.test(() => { return h.mul(h.$(0, 0, 0, 1), h.$(0, 1, 0, 0)).toString() }, 'j(1)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_39', false)
testDriver.test(() => { return h.mul(h.$(0, 1, 0, 0), h.$(0, 0, 0, 1)).toString() }, 'j(-1)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_40', false)

// In-place multiplication
q1 = h.$(1, 2, 3, 4)
q2 = h.$(1, -2, -3, -4)
// new object is not generated
q1 = h.imul(q1, q2)
testDriver.test(() => { return q1.toString() }, '30', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_41', false)

// Division
q1 = h.$(1, 2, 3, 4)
q2 = h.$(1, 2, 3, 4)
// new object is generated
q3 = h.div(q1, q2)
testDriver.test(() => { return q3.toString() }, '1', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_42', false)

// In-place division
q1 = h.$(1, 2, 3, 4)
q2 = h.$(1, 2, 3, 4)
// new object is not generated
q1 = h.idiv(q1, q2)
testDriver.test(() => { return q1.toString() }, '1', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_43', false)

// Multiplication by -1
q1 = h.$(1, 2, 3, 4)
// new object is generated
q2 = h.neg(q1)
testDriver.test(() => { return q2.toString() }, '-1 + i(-2) + j(-3) + k(-4)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_44', false)

// In-place multiplication by -1
q1 = h.$(1, 2, 3, 4)
// new object is not generated
q1 = h.ineg(q1)
testDriver.test(() => { return q1.toString() }, '-1 + i(-2) + j(-3) + k(-4)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_45', false)

// Conjugate
q1 = h.$(1, 2, 3, 4)
// new object is generated
q2 = h.cjg(q1)
testDriver.test(() => { return q2.toString() }, '1 + i(-2) + j(-3) + k(-4)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_46', false)

// In-place evaluation of the conjugate
q1 = h.$(1, 2, 3, 4)
// new object is not generated
q1 = h.icjg(q1)
testDriver.test(() => { return q1.toString() }, '1 + i(-2) + j(-3) + k(-4)', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_47', false)

// Square of the absolute value
q1 = h.$(1, 2, 3, 4)
let a = h.abs2(q1)
testDriver.test(() => { return a.toString() }, '30', 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_48', false)
// return value is not a quaternion (but a real number)
testDriver.test(() => { return a.re }, undefined, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_49', false)
testDriver.test(() => { return a.im }, undefined, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_50', false)

// JSON (stringify and parse)
q1 = h.$(1, 2, 3, 4)
let str = JSON.stringify(q1)
q2 = JSON.parse(str, h.reviver)
testDriver.test(() => { return h.eq(q1, q2) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra-example0_51', false)
