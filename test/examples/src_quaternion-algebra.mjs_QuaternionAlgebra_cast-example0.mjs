import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { Quaternion as H, QuaternionAlgebra }
  from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

let q1 = h.$(1, 0, 0, 0)
let q2 = q1

testDriver.test(() => { return q2 === h.cast(q1) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#cast-example0_0', false)
testDriver.test(() => { return h.cast(1) instanceof H }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#cast-example0_1', false)
testDriver.test(() => { return q2 === h.cast(1) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra#cast-example0_2', false)
testDriver.test(() => { return h.eq(q1, h.cast(1)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#cast-example0_3', false)

testDriver.test(() => { return h.eq(h.cast({ re: 2, im: [1, 2, 3] }), new H(2, 1, 2, 3)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#cast-example0_4', false)
