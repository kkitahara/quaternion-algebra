import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Quaternion as H } from '../../src/index.mjs'

let a = 1 / 2 * Math.sqrt(5)
let b = a - 1
let z = new H(a, b, 3, 4)
let s = JSON.stringify(z)

let w = JSON.parse(s, H.reviver)

testDriver.test(() => { return typeof s }, 'string', 'src/quaternion.mjs~Quaternion.reviver-example0_0', false)
testDriver.test(() => { return w instanceof H }, true, 'src/quaternion.mjs~Quaternion.reviver-example0_1', false)
testDriver.test(() => { return w !== z }, true, 'src/quaternion.mjs~Quaternion.reviver-example0_2', false)
testDriver.test(() => { return z.re === w.re }, true, 'src/quaternion.mjs~Quaternion.reviver-example0_3', false)
testDriver.test(() => { return z.im[0] === w.im[0] }, true, 'src/quaternion.mjs~Quaternion.reviver-example0_4', false)
testDriver.test(() => { return z.im[1] === w.im[1] }, true, 'src/quaternion.mjs~Quaternion.reviver-example0_5', false)
testDriver.test(() => { return z.im[2] === w.im[2] }, true, 'src/quaternion.mjs~Quaternion.reviver-example0_6', false)

let s2 = s.replace('1.0.0', '0.0.0')
testDriver.test(() => { return JSON.parse(s2, H.reviver) }, Error, 'src/quaternion.mjs~Quaternion.reviver-example0_7', false)
