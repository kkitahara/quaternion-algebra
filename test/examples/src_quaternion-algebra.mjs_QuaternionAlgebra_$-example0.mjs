import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { Quaternion as H, QuaternionAlgebra }
  from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

let q = h.$(1, 2, 3, 4)
testDriver.test(() => { return q instanceof H }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#$-example0_0', false)
testDriver.test(() => { return r.eq(q.re, 1) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#$-example0_1', false)
testDriver.test(() => { return r.eq(q.im[0], 2) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#$-example0_2', false)
testDriver.test(() => { return r.eq(q.im[1], 3) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#$-example0_3', false)
testDriver.test(() => { return r.eq(q.im[2], 4) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#$-example0_4', false)

q = h.$()
testDriver.test(() => { return q instanceof H }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#$-example0_5', false)
testDriver.test(() => { return r.eq(q.re, 0) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#$-example0_6', false)
testDriver.test(() => { return r.eq(q.im[0], 0) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#$-example0_7', false)
testDriver.test(() => { return r.eq(q.im[1], 0) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#$-example0_8', false)
testDriver.test(() => { return r.eq(q.im[2], 0) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#$-example0_9', false)
