import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { QuaternionAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

testDriver.test(() => { return h.isInteger(h.$(r.$(0, 2, 5), 2, 3, 4)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isInteger-example0_0', false)
testDriver.test(() => { return h.isInteger(h.$(1, r.$(1, 2, 5), 3, 4)) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isInteger-example0_1', false)
testDriver.test(() => { return h.isInteger(h.$(1, 2, 3, r.$(1, 2, 5))) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isInteger-example0_2', false)
testDriver.test(() => { return h.isInteger(h.$(1, 2, r.$(4, 2, 1), 4)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isInteger-example0_3', false)
