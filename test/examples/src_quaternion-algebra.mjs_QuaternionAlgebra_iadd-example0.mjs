import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { Quaternion as H, QuaternionAlgebra }
  from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

let q1 = h.$(1, 2, 3, 4)
let q2 = h.$(2, 3, -4, 5)
let q3 = q1

// GOOD-PRACTICE!
q1 = h.iadd(q1, q2)
testDriver.test(() => { return q1 === q3 }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#iadd-example0_0', false)
testDriver.test(() => { return q1 instanceof H }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#iadd-example0_1', false)
testDriver.test(() => { return h.eq(q1, new H(3, 5, -1, 9)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#iadd-example0_2', false)
