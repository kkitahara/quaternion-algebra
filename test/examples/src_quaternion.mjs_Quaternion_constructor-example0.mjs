import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Quaternion as H } from '../../src/index.mjs'

let a = new H(1, 2, 3, 4)
testDriver.test(() => { return a.re === 1 }, true, 'src/quaternion.mjs~Quaternion#constructor-example0_0', false)
testDriver.test(() => { return a.im[0] === 2 }, true, 'src/quaternion.mjs~Quaternion#constructor-example0_1', false)
testDriver.test(() => { return a.im[1] === 3 }, true, 'src/quaternion.mjs~Quaternion#constructor-example0_2', false)
testDriver.test(() => { return a.im[2] === 4 }, true, 'src/quaternion.mjs~Quaternion#constructor-example0_3', false)
