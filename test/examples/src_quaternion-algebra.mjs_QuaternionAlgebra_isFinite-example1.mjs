import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { QuaternionAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

let a = 1e999
testDriver.test(() => { return h.isFinite(h.$(r.$(1, 2, 5), 2, 3, 4)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isFinite-example1_0', false)
testDriver.test(() => { return h.isFinite(h.$(r.$(a, 2, 5), 2, 3, 4)) }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isFinite-example1_1', false)
