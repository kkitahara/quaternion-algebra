import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { QuaternionAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

testDriver.test(() => { return h.isExact() }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isExact-example1_0', false)
