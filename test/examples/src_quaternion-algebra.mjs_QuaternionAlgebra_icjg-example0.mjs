import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { Quaternion as H, QuaternionAlgebra }
  from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

let q1 = h.$(1, 2, 3, 4)
let q2 = q1

// GOOD-PRACTICE!
q1 = h.icjg(q1)
testDriver.test(() => { return q1 === q2 }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#icjg-example0_0', false)
testDriver.test(() => { return q1 instanceof H }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#icjg-example0_1', false)
testDriver.test(() => { return h.eq(q1, new H(1, -2, -3, -4)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#icjg-example0_2', false)
