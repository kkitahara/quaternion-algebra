import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { QuaternionAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

testDriver.test(() => { return r.isReal() }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isReal-example0_0', false)
testDriver.test(() => { return h.isReal() }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isReal-example0_1', false)
