import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { Quaternion as H } from '../../src/index.mjs'
let r = new RealAlgebra()

let a = r.iadd(r.$(-1, 2), r.$(-1, 2, 5))
let b = r.iadd(r.$(-1), a)
let z = new H(a, b, 3, 4)

// toJSON method is called by JSON.stringify
let s = JSON.stringify(z)

testDriver.test(() => { return typeof s }, 'string', 'src/quaternion.mjs~Quaternion#toJSON-example0_0', false)
