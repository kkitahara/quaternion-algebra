import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { QuaternionAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

let q1 = h.$(1, 2, r.$(1, 2, 5), 4)
let str = JSON.stringify(q1)
let q2 = JSON.parse(str, h.reviver)
testDriver.test(() => { return h.eq(q1, q2) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#reviver-example0_0', false)
