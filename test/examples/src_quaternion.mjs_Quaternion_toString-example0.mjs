import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { Quaternion as H } from '../../src/index.mjs'
let r = new RealAlgebra()

testDriver.test(() => { return new H(1, 2, 3, 4).toString() }, '1 + i(2) + j(3) + k(4)', 'src/quaternion.mjs~Quaternion#toString-example0_0', false)
testDriver.test(() => { return new H(0, 0, 0, 0).toString() }, '0', 'src/quaternion.mjs~Quaternion#toString-example0_1', false)
testDriver.test(() => { return new H(0, 0, 0, 1).toString() }, 'k(1)', 'src/quaternion.mjs~Quaternion#toString-example0_2', false)
testDriver.test(() => { return new H(1, 0, 0, 1).toString() }, '1 + k(1)', 'src/quaternion.mjs~Quaternion#toString-example0_3', false)
testDriver.test(() => { return new H(0, 0, 0, r.$(1, 2, 5)).toString() }, 'k((1 / 2)sqrt(5))', 'src/quaternion.mjs~Quaternion#toString-example0_4', false)
testDriver.test(() => { return new H(1, 0, 0, r.$(1, 2)).toString() }, '1 + k(1 / 2)', 'src/quaternion.mjs~Quaternion#toString-example0_5', false)
testDriver.test(() => { return new H(1, 0, 0, r.$(0)).toString() }, '1', 'src/quaternion.mjs~Quaternion#toString-example0_6', false)
testDriver.test(() => { return new H(r.$(1, 3), 0, 0, 1).toString() }, '1 / 3 + k(1)', 'src/quaternion.mjs~Quaternion#toString-example0_7', false)
testDriver.test(() => { return new H(r.$(0), 0, 1, 0).toString() }, 'j(1)', 'src/quaternion.mjs~Quaternion#toString-example0_8', false)
