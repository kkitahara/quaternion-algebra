import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { Quaternion as H, QuaternionAlgebra }
  from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

let q = h.$(1, 2, 3, 4)

let a = h.abs(q)
testDriver.test(() => { return a instanceof H }, false, 'src/quaternion-algebra.mjs~QuaternionAlgebra#abs-example1_0', false)
testDriver.test(() => { return typeof a === 'number' }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#abs-example1_1', false)
testDriver.test(() => { return h.eq(q, new H(1, 2, 3, 4)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#abs-example1_2', false)
testDriver.test(() => { return r.eq(a, Math.sqrt(30)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#abs-example1_3', false)
