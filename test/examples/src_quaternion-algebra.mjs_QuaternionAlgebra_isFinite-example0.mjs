import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { QuaternionAlgebra } from '../../src/index.mjs'
import bigInt from 'big-integer'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

let a = bigInt('1e999')
testDriver.test(() => { return h.isFinite(h.$(r.$(1, 2, 5), 2, 3, 4)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isFinite-example0_0', false)
testDriver.test(() => { return h.isFinite(h.$(r.$(a, 2, 5), 2, 3, 4)) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#isFinite-example0_1', false)
