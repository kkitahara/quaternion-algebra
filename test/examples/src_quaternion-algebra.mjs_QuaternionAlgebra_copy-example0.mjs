import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { Quaternion as H, QuaternionAlgebra }
  from '../../src/index.mjs'
let r = new RealAlgebra()
let h = new QuaternionAlgebra(r)

let q1 = h.$(1, 0, 0, 0)
let q2 = h.copy(q1)

testDriver.test(() => { return q1 instanceof H }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#copy-example0_0', false)
testDriver.test(() => { return q2 instanceof H }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#copy-example0_1', false)
testDriver.test(() => { return r.eq(q1.re, q2.re) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#copy-example0_2', false)
testDriver.test(() => { return r.eq(q1.im[0], q2.im[0]) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#copy-example0_3', false)
testDriver.test(() => { return r.eq(q1.im[1], q2.im[1]) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#copy-example0_4', false)
testDriver.test(() => { return r.eq(q1.im[2], q2.im[2]) }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#copy-example0_5', false)
testDriver.test(() => { return q1 !== q2 }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#copy-example0_6', false)
testDriver.test(() => { return q1.re !== q2.re }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#copy-example0_7', false)
testDriver.test(() => { return q1.im !== q2.im }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#copy-example0_8', false)
testDriver.test(() => { return q1.im[0] !== q2.im[0] }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#copy-example0_9', false)
testDriver.test(() => { return q1.im[1] !== q2.im[1] }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#copy-example0_10', false)
testDriver.test(() => { return q1.im[2] !== q2.im[2] }, true, 'src/quaternion-algebra.mjs~QuaternionAlgebra#copy-example0_11', false)
