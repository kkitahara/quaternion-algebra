import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { Quaternion as H } from '../../src/index.mjs'
let r = new RealAlgebra()

let a = r.iadd(r.$(-1, 2), r.$(-1, 2, 5))
let b = r.iadd(r.$(-1), a)
let z = new H(a, b, 3, 4)
testDriver.test(() => { return z.toFixed(3) }, '-1.618 + i(-2.618) + j(3.000) + k(4.000)', 'src/quaternion.mjs~Quaternion#toFixed-example0_0', false)
testDriver.test(() => { return new H(0, 0, 0, 0).toFixed(3) }, '0.000', 'src/quaternion.mjs~Quaternion#toFixed-example0_1', false)
testDriver.test(() => { return new H(0, 0, 0, 1).toFixed(3) }, 'k(1.000)', 'src/quaternion.mjs~Quaternion#toFixed-example0_2', false)
testDriver.test(() => { return new H(0, 0, 0, r.$(1)).toFixed(3) }, 'k(1.000)', 'src/quaternion.mjs~Quaternion#toFixed-example0_3', false)
testDriver.test(() => { return new H(1, 0, 0, r.$(1)).toFixed(3) }, '1.000 + k(1.000)', 'src/quaternion.mjs~Quaternion#toFixed-example0_4', false)
testDriver.test(() => { return new H(r.$(0), 0, 0, r.$(0)).toFixed(3) }, '0.000', 'src/quaternion.mjs~Quaternion#toFixed-example0_5', false)
